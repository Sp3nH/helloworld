public class Car {

    //attributes
    private String make;
    private String model;
    private String colour;
    int age;

    public void print() {
        System.out.println("Car details");
        System.out.println(make + " " + model + model + age + colour);
    }



    //constructor (s) - constructor is a special method which creates an object from your class
    public Car(String make, String model, String colour, int age) {

        this.make = make;
        this.model = model;
        this.colour = colour;
        this.age = age;
    }


    //methods
                //getter
    public String getMake() {

        return make;
    }

                //setter
    public void setModel(String m) {

        model = m;
    }


}
