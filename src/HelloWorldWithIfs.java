import java.util.Scanner;

public class HelloWorldWithIfs {

    public static void main(String[] args) {

        System.out.println("Enter your first name");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();

        System.out.println("Hello " + name + ", this is JAVA!");
        scanner.close();

        int x = 7;

        if (x < 10) {
            System.out.println("X is less than 10");
        } else if (x <= 100) {
            System.out.println("X is between 10 and 100");
        } else {
            System.out.println("X is greater than 100");
        }

        //want to know how many cats and how many dogs are in the array
        String[] animals = {"cat", "cat", "dog", "tortoise", "cat", "rabbit", "dog", "cat", "dog", "cat"};

        int numberOfDogs = 0;
        int numberOfCats = 0;
        int numberOfOther = 0;
        int arrayLength = animals.length;

        for (int i = 0; i < animals.length; i++) {
            if (animals[i] == "dog") {
                numberOfDogs++;
            }

            if (animals[i] == "dog") {
                numberOfOther++;
            }

            if (animals[i] == "cat") {
                numberOfCats++;
            } else if (animals[i] != "cat" && animals[i] != "dog") {
                numberOfOther++;
            }

        }
        System.out.println("The number of dogs is " + numberOfDogs + ". The number of cats is " + numberOfCats + ".");
        System.out.println("The number of other animals is " + numberOfOther + ".");


        int[] ages = {24, 31, 29, 40, 18, 20, 42, 50};

        int maxAge = ages[0];
        int minAge = ages[0];

        for (int i = 1; i < ages.length; i++) {
            if (ages[i] > maxAge) {
                maxAge = ages[i];
            }
        }

        for (int i = 1; i < ages.length; i++) {
            if (ages[i] < minAge) {
                minAge = ages[i];
            }
        }
        System.out.println("The maximum age is " + maxAge + ". The minimum age is " + minAge + ".");




        //Classes are initialised with the 'new' keyword - then we call the constructor

        Car db5 = new Car("Aston Martin", "DB5", "Silver", 56);
        //"Car()" is the constructor, creating a Car object from the Car class. This Car object is assigned db5, and then "new Car" creates it

        db5.print();
        //the "." allows us to access the properties of db5 and run the method "print()" from the Car class.

    }

}
