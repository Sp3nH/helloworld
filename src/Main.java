import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        System.out.println("Enter your first name");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();

        System.out.println("Hello " + name + ", this is JAVA!");
        scanner.close();


        //using a for loop below to introduce an array with numbers.

        // the next 3 values determine the array!!
        int numberOfElements = 10; //setting the number of elements in the array (ARRAY SIZE)
        int startingNumber = 1; // setting te starting point for the loop. This is what will determine our values
        int increment = 12; //setting the value it increases by.
        int multiple = 3;

        int[] numbers = new int[numberOfElements]; //defining an array using numberOfElements variable as the array size in the declaration.
        // the array is type in, named numbers and uses a variable to determine the array size
        int currentNumber = startingNumber; //the current number is what will change in the loop.

        for (int i = 0; i < numberOfElements; i++) //our loop which creates the array
        {
            numbers[i] = currentNumber; //the array index = current number - therefore the current number is put into the array at that index
            currentNumber += increment; // then the current number increases by "increment" variable
        }


        // this for loop which prints out all the values in the array

        for (int i = 0; i < numbers.length; i++)
        {
            System.out.println("Numbers: " + numbers[i]);
        }

        // will times all values by the multiple

        for (int i = 0; i < numbers.length; i++)
        {
            numbers[i] *= multiple;
        }

        // this for loop which prints out all the values in the array

        for (int i = 0; i < numbers.length; i++)
        {
            System.out.println("Numbers: " + numbers[i]);
        }


        //the below for loop will sum all the numbers in the array - in this case it is 1650 because the numbers have been incremented by 12,
        //then x3.

        int total = 0; // creates a variable which is temporarily assigned 0
        for (int i = 0; i < numbers.length; i++) //classic for loop which allows us to cycle through the values
        {
            total += numbers[i]; //the action of the loop this time is to take the variable "total" (=0) and to add the value at the specified
            //index of the array.
        }
        System.out.println("Total is: " + total);





    }
}
